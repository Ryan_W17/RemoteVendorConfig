from Vendors import CiscoVendor, JuniperVendor
from utils import Constants


cisco = CiscoVendor.Vendor("host", 22, "username", "password", Constants.ConnectionType.SSH)
cisco_config = cisco.get_config()
cisco_sub_sections = cisco.get_config_sections("interface FastEthernet0/")



juniper = JuniperVendor.Vendor("host", 23, "username", "password", Constants.ConnectionType.TELNET)
juniper_config = juniper.get_config()