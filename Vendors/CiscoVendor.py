import time
import paramiko
import re
from Vendors import AbstractVendor
from utils import Constants
from netmiko import ConnectHandler


class Vendor(AbstractVendor.VendorData):

    def __init__(self, host, port, username, password, contype):
        super().__init__(host, port, username, password, contype)

    def get_config(self):
        if self._contype == Constants.ConnectionType.SSH:
            pre_connect = paramiko.SSHClient()
            pre_connect.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            pre_connect.connect(self._host, self._port, self._username, self._password, look_for_keys=False, allow_agent=False)
            print(">> Connected to remote Cisco IOS Device...")
            connect = pre_connect.invoke_shell()
            connect.send("show run")
            print(">> Getting IOS running configuration")
            time.sleep(2)
            config = connect.recv(65535)
            connect.close()
            pre_connect.close()
            return config
        elif self._contype == Constants.ConnectionType.TELNET:
            device = {
                'device_type': 'cisco_ios_telnet',
                'ip': self._host,
                'username': self._username,
                'password': self._password,
            }
            net_con = ConnectHandler(**device)
            print(">> Connected to remote Cisco IOS Device...")
            print(">> Getting IOS running configuration")
            output = net_con.send_command("show run")
            return output

    def get_config_sections(self, sectionRegex):
        children = {}
        config_split = self.get_config().split("\n")
        for line in config_split:
            if re.match(sectionRegex, line):
                matched_index = config_split.index(line)
                for following_lines in config_split[matched_index:]:
                    if str(following_lines).startswith("!"):
                        break
                    if str(following_lines).startswith(" "): # If the current line is a child
                        if not config_split[matched_index] in children:
                            children[config_split[matched_index]] = [str(following_lines).lstrip().replace("\n", "")]
                        else:
                            found_children = children[config_split[matched_index]]
                            found_children.append(str(following_lines).lstrip().replace("\n", ""))
                            children[config_split[matched_index]] = found_children
        return children