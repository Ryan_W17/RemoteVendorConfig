from abc import abstractmethod

"""
Abstract VendorData class for other classes to implement and use the abstract methods.
Outlines a basic structure to follow
__init__ method, takes the host, port, username, pass and connection type of the 
remote device you're trying to connect to
contype is the Connection type, so their Constants.ConnectionType.SSH or Constants.ConnectionType.TELNET
"""


class VendorData(object):

    @abstractmethod
    def __init__(self, host, port, username, password, contype):
        self._host = host
        self._port = port
        self._username = username
        self._password = password
        self._contype = contype

    @abstractmethod
    def get_config(self):
        return

    @abstractmethod
    def get_config_sections(self, sectionRegex):
        return

