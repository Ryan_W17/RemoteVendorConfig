from Vendors import AbstractVendor
from jnpr.junos import Device


class Vendor(AbstractVendor.VendorData):
    def __init__(self, host, port, username, password, contype):
        super().__init__(host, port, username, password, contype)

    def get_config(self):
        junos_dev = Device(host=self._host + ":" + str(self._port), user=self._username, password=self._password)
        junos_dev.open()
        config = junos_dev.cli("show configuration")
        junos_dev.close()
        return config
        pass

    def get_config_section(self):
        pass
